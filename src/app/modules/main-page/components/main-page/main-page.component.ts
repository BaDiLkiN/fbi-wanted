import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '../../../../shared/classes/post';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { HttpService } from '../../../../shared/services/http.service';
import { environment } from '../../../../../environments/environment';

@UntilDestroy()
@Component({
  selector: 'fw-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MainPageComponent implements OnInit, OnDestroy {
  public posts: Post[];

  constructor(private cdRef: ChangeDetectorRef, private http: HttpService) {
  }

  public ngOnInit(): void {
    this.viewPosts();
  }

  private getPosts(): Observable<Post[]> {
    return this.http.get(environment.posts);
  }

  private viewPosts(): void {
    this.getPosts()
      .pipe(untilDestroyed(this))
      .subscribe((data: Post[]) => {
        this.posts = data;
        this.cdRef.markForCheck();
      })
  }

  public ngOnDestroy(): void {
  }
}
