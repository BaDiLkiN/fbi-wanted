import { NgModule } from '@angular/core';
import { MainPageComponent } from './components/main-page/main-page.component';
import { BrowserModule } from '@angular/platform-browser';
import { mainPageRouting } from './main-page.routing';

@NgModule({
  declarations: [MainPageComponent],
  imports: [
    BrowserModule,
    mainPageRouting
  ]
})
export class MainPageModule {

}


