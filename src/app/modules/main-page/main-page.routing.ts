import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { MainPageComponent } from './components/main-page/main-page.component';

export const mainPageRoutes: Routes = [
  {path: 'main', component: MainPageComponent}
];

export const mainPageRouting: ModuleWithProviders<RouterModule> = RouterModule.forChild(mainPageRoutes);
