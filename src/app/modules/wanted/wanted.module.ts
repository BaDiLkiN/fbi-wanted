import { NgModule } from '@angular/core';
import { WantedComponent } from './components/wanted/wanted.component';
import { wantedRouting } from './wanted.routing';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { WantedDetailsComponent } from './components/wanted-details/wanted-details.component';
import { DefaultResponseValuesPipe } from '../../shared/pipes/default-response-values.pipe';
import { MatSortModule } from '@angular/material/sort';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatTooltipModule } from '@angular/material/tooltip';
import { WantedDataToEditComponent } from './components/wanted-data-to-edit/wanted-data-to-edit.component';
import { WantedCorrectiveComponent } from './components/wanted-corrective/wanted-corrective.component';
import { MatStepperModule } from '@angular/material/stepper';
import { MatRadioModule } from '@angular/material/radio';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
  declarations: [WantedComponent, WantedDetailsComponent, DefaultResponseValuesPipe, WantedDataToEditComponent, WantedCorrectiveComponent],
  imports: [
    wantedRouting,
    MatButtonModule,
    MatTableModule,
    MatInputModule,
    FormsModule,
    CommonModule,
    MatPaginatorModule,
    MatSortModule,
    MatSlideToggleModule,
    MatCardModule,
    MatDividerModule,
    ReactiveFormsModule,
    MatTooltipModule,
    MatStepperModule,
    MatRadioModule,
    MatDialogModule
  ],
  entryComponents: [WantedCorrectiveComponent]
})
export class WantedModule {

}
