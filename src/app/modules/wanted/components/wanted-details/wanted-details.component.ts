import { ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import { WantedPerson } from '../../../../shared/interfaces/wanted-person.interface';

@UntilDestroy()
@Component({
  selector: 'fw-wanted-details',
  templateUrl: './wanted-details.component.html',
  styleUrls: ['./wanted-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class WantedDetailsComponent implements OnInit, OnDestroy {

  @Input() selectedUser: WantedPerson;

  constructor() {
  }

  public ngOnInit(): void {
  }

  public ngOnDestroy(): void {
  }

}
