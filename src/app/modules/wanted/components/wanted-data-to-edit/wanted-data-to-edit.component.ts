import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { WantedPerson } from '../../../../shared/interfaces/wanted-person.interface';
import { ActivatedRoute } from '@angular/router';
import { WantedService } from '../../../../shared/services/wanted.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'fw-wanted-data-to-edit',
  templateUrl: './wanted-data-to-edit.component.html',
  styleUrls: ['./wanted-data-to-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WantedDataToEditComponent implements OnInit {

  public displayedColumns: string[] = ['num', 'title', 'sex', 'hair', 'eyes', 'weight', 'dates_of_birth_used', 'description', 'url', 'warning_message'];

  public updatedWanted: WantedPerson[];
  public selectedWantedPerson: WantedPerson;

  constructor(
    private route: ActivatedRoute,
    private wantedService: WantedService,
    private cdRef: ChangeDetectorRef,
  ) {
  }

  public ngOnInit(): void {
    this.loadUpdatedWanted();
  }

  private loadUpdatedWanted(): void {
    this.wantedService.getUpdatedWanted()
      .pipe(untilDestroyed(this))
      .subscribe(updatedResponse => {
        this.updatedWanted = updatedResponse;
        this.cdRef.markForCheck();
      })
  }

  public onRowClicked(row: WantedPerson): void {
    this.selectedWantedPerson = row;
  }
}
