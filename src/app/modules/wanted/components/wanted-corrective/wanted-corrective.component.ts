import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatStepperIntl } from '@angular/material/stepper';
import { UntilDestroy } from '@ngneat/until-destroy';
import { WantedPerson } from '../../../../shared/interfaces/wanted-person.interface';
import { WantedService } from '../../../../shared/services/wanted.service';
import { HttpService } from '../../../../shared/services/http.service';
import { MatDialogRef } from '@angular/material/dialog';
import { SexOptions } from '../../../../shared/enums/sex.options';

@UntilDestroy()
@Component({
  selector: 'fw-wanted-corrective',
  templateUrl: './wanted-corrective.component.html',
  styleUrls: ['./wanted-corrective.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{provide: MatStepperIntl}]
})
export class WantedCorrectiveComponent implements OnInit {
  public existWanted = this.wantedService.getExistWantedDetails();
  public editedWantedDetailsFormGroup: FormGroup;
  public secondFormGroup: FormGroup;
  public optionalLabelTextChoices: string[] = Object.keys(SexOptions);

  constructor(
    private _formBuilder: FormBuilder,
    private _matStepperIntl: MatStepperIntl,
    private cdRef: ChangeDetectorRef,
    private wantedService: WantedService,
    private http: HttpService,
    private dialogRef: MatDialogRef<any>
  ) {
  }

  public ngOnInit(): void {
    this.buildEditForm(this.existWanted);
  }

  public buildEditForm(wantedDetails: WantedPerson): void {
    this.editedWantedDetailsFormGroup = this._formBuilder.group({
      title: ['', Validators.required],
      hair: [''],
      eyes: [''],
      weight: [''],
      birthDate: [''],
      description: [''],
      warning: [''],
    });
    this.fillWantedForm(wantedDetails);

    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.cdRef.markForCheck();
  }

  private fillWantedForm(wantedDetails: WantedPerson): void {
    if (wantedDetails) {
      this.editedWantedDetailsFormGroup.patchValue({
        title: [wantedDetails.title],
        hair: [wantedDetails.hair],
        eyes: [wantedDetails.eyes],
        weight: [wantedDetails.weight],
        birthDate: [wantedDetails.dates_of_birth_used],
        description: [wantedDetails.description],
        warning: [wantedDetails.warning],
      })
    }
  }

  public updateWanted(): any {
    let body = this.editedWantedDetailsFormGroup.value;
    return this.http.post(body);
  }

  public undoUpdateWanted(): void {
    this.wantedService.clearExistWantedDetails();
    this.dialogRef.close();
  }

}
