import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import { WantedService } from '../../../../shared/services/wanted.service';
import { MatTableDataSource } from '@angular/material/table';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { WantedPerson } from '../../../../shared/interfaces/wanted-person.interface';
import { MatSort } from '@angular/material/sort';
import { FormControl, FormGroup } from '@angular/forms';
import { FilterParams } from '../../../../shared/interfaces/filter-params.interface';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { WantedCorrectiveComponent } from '../wanted-corrective/wanted-corrective.component';

@UntilDestroy()
@Component({
  selector: 'fw-wanted',
  templateUrl: './wanted.component.html',
  styleUrls: ['./wanted.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class WantedComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  public displayedColumns: string[] = ['num', 'title', 'sex', 'hair', 'eyes', 'weight', 'dates_of_birth_used', 'description', 'url', 'warning_message'];

  public formFilter: FormGroup;
  public filterControl: FormControl = new FormControl();
  public data: WantedPerson[];
  public dataSource: MatTableDataSource<any>;
  public selectedWantedPerson: WantedPerson;
  public selectedRowIndex: string = '-1';
  public result: any = [];
  public currentPage: number = 1;
  public lastPage: number;

  constructor(
    private cdRef: ChangeDetectorRef,
    private wantedService: WantedService,
    public dialog: MatDialog
  ) {
  }

  public ngOnInit(): void {
    this.buildFilterForm();
    this.getFilteredData();
  }

  public buildFilterForm(): void {
    this.formFilter = new FormGroup(
      {
        office: new FormControl(''),
        sex: new FormControl(''),
        title: new FormControl(''),
      }
    )
  }

  public getFilteredData(): void {
    const params: FilterParams = this.getFilterParams();

    this.wantedService.getFilteredWanted(params)
      .pipe(
        untilDestroyed(this)
      )
      .subscribe(data => {
          this.data = data.items;
          this.lastPage = Math.ceil(data.total / 20);
          this.dataSource = new MatTableDataSource(this.data);
          this.dataSource.sort = this.sort;
          this.cdRef.markForCheck();
        }
      );
    this.cdRef.markForCheck();
  }

  private getFilterParams(): FilterParams {
    return {
      page: this.currentPage,
      office: this.formFilter.value.office,
      sex: this.formFilter.value.sex,
      title: this.formFilter.value.title
    }
  }

  public onRowClicked(row: WantedPerson): void {
    this.selectedWantedPerson = row;
  }

  public ngAfterViewInit(): void {
    this.cdRef.markForCheck();
  }

  public closeDetails(): void {
    this.selectedWantedPerson = null;
    this.selectedRowIndex = '-1';
  }

  public getNextPageWanted(): void {
    if (this.currentPage === this.lastPage) {
      return;
    }
    this.currentPage++;
    this.getFilteredData();
    this.cdRef.markForCheck();
  }

  public getPreviousPageWanted(): void {
    if (this.currentPage === 1) {
      return;
    }
    this.currentPage--;
    this.getFilteredData();
    this.cdRef.markForCheck();

  }

  public openDialog(): void {
    this.wantedService.setExistWantedDetails(this.selectedWantedPerson);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '60%';
    this.dialog.open(WantedCorrectiveComponent, dialogConfig);
  }


  public ngOnDestroy(): void {
  }
}

