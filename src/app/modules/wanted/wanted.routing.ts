import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { WantedComponent } from './components/wanted/wanted.component';
import { AuthGuard } from '../../shared/guards/auth.guard';
import { WantedDataToEditComponent } from './components/wanted-data-to-edit/wanted-data-to-edit.component';

export const wantedRoutes: Routes = [
  {path: 'wanted', component: WantedComponent, canActivate: [AuthGuard], pathMatch: 'full'},
  {path: 'wanted/edit/:uid', component: WantedDataToEditComponent, canActivate: [AuthGuard], pathMatch: 'full'}
];

export const wantedRouting: ModuleWithProviders<RouterModule> = RouterModule.forChild(wantedRoutes);
