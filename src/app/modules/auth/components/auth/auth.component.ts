import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../../shared/services/auth.service';
import { User } from '../../../../shared/classes/user';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LoginOptions } from '../../../../shared/enums/login.options'

@UntilDestroy()
@Component({
  selector: 'fw-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class AuthComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public login: string;
  public loginOptions = Object.keys(LoginOptions);

  constructor(
    private auth: AuthService,
    private route: Router,
    private snackBar: MatSnackBar,
    private cdRef: ChangeDetectorRef,
  ) {
  }

  public ngOnInit(): void {
    this.buildForm();
    this.currentUserListener();
    this.cdRef.markForCheck();
  }

  public submit(): void {
    const {login, password} = this.form.value;
    this.auth
      .login(login, password)
      .pipe(untilDestroyed(this))
      .subscribe(user => {
        if (user) {
          this.route.navigate(['']);
        } else {
          this.snackBar.open('Wrong login or password!', 'OK', {duration: 3000});
          this.form.reset();
        }
      })
    this.cdRef.markForCheck();
  }

  public buildForm(): void {
    this.form = new FormGroup(
      {
        login: new FormControl('', Validators.required),
        password: new FormControl(null, Validators.required)
      }
    );
  }

  public currentUserListener(): void {
    this.auth.currentUser.pipe(untilDestroyed(this)).subscribe((user: User) => {
      this.login = user ? user.login : null;
      this.cdRef.markForCheck();
    });
  }

  public userLogout(): void {
    this.auth.logout();
  }

  public isUserLogged(): boolean {
    return this.auth.isAuth;
  }

  public ngOnDestroy(): void {
  }
}


