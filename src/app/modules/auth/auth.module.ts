import { NgModule } from '@angular/core';
import { AuthComponent } from './components/auth/auth.component';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { authRouting } from './auth.routing';

@NgModule({
  declarations: [
    AuthComponent,
  ],
  imports:
    [
      BrowserModule,
      ReactiveFormsModule,
      BrowserAnimationsModule,
      MatCardModule,
      MatInputModule,
      MatButtonModule,
      MatSnackBarModule,
      authRouting
    ]
})
export class AuthModule {
}
