import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { AuthComponent } from './components/auth/auth.component';

export const authRoutes: Routes = [
  {path: 'auth', component: AuthComponent}
];

export const authRouting: ModuleWithProviders<RouterModule> = RouterModule.forChild(authRoutes);
