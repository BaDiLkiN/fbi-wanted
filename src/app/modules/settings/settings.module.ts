import { NgModule } from '@angular/core';
import { SettingsComponent } from './components/settings/settings.component';
import { settingsRouting } from './settings.routing';

@NgModule({
  declarations: [SettingsComponent],
  imports: [
    settingsRouting
  ]
})
export class SettingsModule {

}
