import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'fw-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy {

  constructor() {
  }

  public ngOnInit(): void {
  }

  public ngOnDestroy(): void {
  }
}
