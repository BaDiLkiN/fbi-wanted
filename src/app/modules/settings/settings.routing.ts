import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../shared/guards/auth.guard';
import { ModuleWithProviders } from '@angular/core';
import { SettingsComponent } from './components/settings/settings.component';

export const settingsRoutes: Routes = [
  {path: 'settings', component: SettingsComponent, canActivate: [AuthGuard], pathMatch: 'full'}
];

export const settingsRouting: ModuleWithProviders<RouterModule> = RouterModule.forChild(settingsRoutes);
