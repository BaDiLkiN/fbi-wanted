import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { WantedPerson } from '../interfaces/wanted-person.interface';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) {
  }

  public get(address: string): Observable<any> {
    return this.http.get(address);
  }

  public getWithParams(address: string, params: any): Observable<any> {
    return this.http.get(address, {params});
  }

  public post(body: WantedPerson): void {
    this.http.post(environment.wantedDB, body).subscribe(response => response);
  }

}
