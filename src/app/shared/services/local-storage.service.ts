import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() {
  }

  public set(item: string, val: string): void {
    localStorage.setItem(item, val);
  }

  public get(item: string): string {
    return localStorage.getItem(item);
  }

  public delete(item: string): void {
    localStorage.removeItem(item);
  }

}
