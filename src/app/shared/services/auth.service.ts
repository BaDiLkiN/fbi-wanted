import { User } from '../classes/user';
import { map, pluck, tap } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { HttpService } from './http.service';
import { LocalStorageService } from './local-storage.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public currentUser = new BehaviorSubject<User>(null);

  public get isAuth(): boolean {
    return Boolean(this.currentUser.value);
  }

  constructor(
    private http: HttpService,
    private route: Router,
    private localStorage: LocalStorageService
  ) {
  }

  public checkUserWasLogged(): void {
    if (this.localStorage.get('user')) {
      const users = this.http.get(environment.users);
      users
        .pipe(
          pluck('users'),
          map((users: User[]) =>
            users.find(user => user.login === this.localStorage.get('user'))
          ),
          untilDestroyed(this)
        )
        .subscribe(user => this.currentUser.next(user));
    } else {
      this.currentUser = new BehaviorSubject<User>(null);
    }
  }

  public login(login: string, password: string): Observable<User> {
    return this.http.get(environment.users)
      .pipe(
        pluck('users'),
        map((users: User[]) =>
          users.find(user => user.login === login && user.password === password)
        ),
        tap((user: User) => {
            this.currentUser.next(user);
            if (user) {
              this.localStorage.set('user', user.login);
            }
          }
        ))
  }

  public logout(): void {
    this.currentUser = new BehaviorSubject<User>(null);
    this.localStorage.delete('user');
    this.route.navigate(['']);
  }
}
