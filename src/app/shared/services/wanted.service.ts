import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Observable, zip } from 'rxjs';
import { map } from 'rxjs/operators';
import { WantedPerson } from '../interfaces/wanted-person.interface';
import { environment } from '../../../environments/environment';
import { FilterParams } from '../interfaces/filter-params.interface';
import { HttpParams } from '@angular/common/http';
import { PagedResponse } from '../interfaces/paged-response.interface';

@Injectable({
  providedIn: 'root'
})
export class WantedService {
  private existWanted: WantedPerson;

  constructor(private http: HttpService) {
  }

  public getFilteredWanted(params?: FilterParams): Observable<PagedResponse<WantedPerson>> {

    const httpParams = this.getHttpParams(params);

    return this.http.getWithParams(environment.apiURL, httpParams);
  };

  public getUpdatedWanted(): Observable<any> {
    const updatedWantedURL = environment.wantedDB;
    return this.http.get(updatedWantedURL);
  }

  public markModifiedWanted(): Observable<any> {
    const wantedResponse: Observable<WantedPerson[]> = this.getFilteredWanted().pipe(
      map(response => response.items)
    );
    const modifiedResponse: Observable<WantedPerson[]> = this.getModifiedWanted();
    return zip(
      wantedResponse,
      modifiedResponse,
    )
      .pipe(
        map(([wantedData, modifiedData]) => {
          const modifiedUIDs = new Set(modifiedData.map(p => p.uid));
          wantedData.map(person => person.isModified = modifiedUIDs.has(person.uid));
          return wantedData;
        }),
      )
  }

  public getModifiedWanted(): Observable<WantedPerson[]> {
    return this.http.get(environment.wantedDB);
  }

  public getHttpParams(params: FilterParams): Object {
    if (!params) {
      return null;
    }
    const httpParams: HttpParams = new HttpParams()
      .set('page', params.page.valueOf())
      .set('field_offices', params.office.toString())
      .set('sex', params.sex.toString())
      .set('title', params.title.toString())
    return httpParams;
  }

  public setExistWantedDetails(wantedData: WantedPerson): void {
    this.existWanted = wantedData;
  }

  public getExistWantedDetails(): WantedPerson {
    const tempWantedDetails = this.existWanted;
    this.clearExistWantedDetails();
    return tempWantedDetails;
  }

  public clearExistWantedDetails(): void {
    this.existWanted = null;
  }

}
