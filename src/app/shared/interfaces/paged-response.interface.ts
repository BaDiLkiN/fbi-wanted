export interface PagedResponse<T> {
  total: number;
  items: T[];
  page: number;
}
