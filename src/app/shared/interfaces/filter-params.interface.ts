export interface FilterParams {
  page: number;
  office: string;
  sex: string;
  title: string;
}
