export enum LoginOptions {
  admin = 'admin',
  user = 'user',
  guest = 'guest',
}
