import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'defaultValues'})
export class DefaultResponseValuesPipe implements PipeTransform {
  transform(value: string | number | Date): any {
    return !value ? ' —' : value;
  }
}
