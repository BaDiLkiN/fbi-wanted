import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { AuthModule } from './modules/auth/auth.module';
import { MainPageModule } from './modules/main-page/main-page.module';
import { WantedModule } from './modules/wanted/wanted.module';
import { SettingsModule } from './modules/settings/settings.module';
import { AppRoutingModule } from './app-routing.module';
import { LayoutModule } from './layout/layout.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    AuthModule,
    MainPageModule,
    SettingsModule,
    WantedModule,
    AppRoutingModule,
    LayoutModule,

    BrowserModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
