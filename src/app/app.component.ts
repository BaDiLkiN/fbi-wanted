import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AuthService } from './shared/services/auth.service';

@Component({
  selector: 'fw-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  title = 'FBI-wanted';

  constructor(private auth: AuthService) {
    this.auth.checkUserWasLogged();
  }

  public isUserLogged(): boolean {
    return this.auth.isAuth;
  }
}

