import { NgModule } from '@angular/core';
import { TopBarModule } from './components/top-bar/top-bar.module';
import { SideBarModule } from './components/side-bar/side-bar.module';

@NgModule({
  imports: [
    TopBarModule,
    SideBarModule,
  ],
  exports: [
    TopBarModule,
    SideBarModule
  ]
})

export class LayoutModule {

}
