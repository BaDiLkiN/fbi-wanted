import { ChangeDetectionStrategy, Component, } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'fw-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SideBarComponent {
  constructor(
    private location: Location
  ) {
  }

  public getBack(): void {
    this.location.back();
  }

}
