import { NgModule } from '@angular/core';
import { SideBarComponent } from './side-bar.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    SideBarComponent
  ],
  exports: [
    SideBarComponent,
  ],
  imports: [
    MatSidenavModule,
    MatButtonModule,
    RouterModule
  ]
})
export class SideBarModule {

}
