import { NgModule } from '@angular/core';
import { TopBarComponent } from './top-bar.component';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [TopBarComponent],
  exports: [
    TopBarComponent
  ],
  imports: [
    RouterModule,
    BrowserModule,
    MatCardModule,
    MatButtonModule
  ]
})

export class TopBarModule {

}
