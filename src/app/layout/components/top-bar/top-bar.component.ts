import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '../../../shared/services/auth.service';
import { environment } from '../../../../environments/environment';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { User } from '../../../shared/classes/user';

@UntilDestroy()
@Component({
  selector: 'fw-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TopBarComponent implements OnInit, OnDestroy {
  public img = environment.logo;

  constructor(
    private auth: AuthService,
    private cdRef: ChangeDetectorRef,
  ) {
  }

  public ngOnInit(): void {
    this.subscribeUser();
  }

  private subscribeUser(): void {
    this.auth.currentUser
      .pipe(untilDestroyed(this))
      .subscribe(() => this.cdRef.markForCheck());
  }

  public logoutUser(): void {
    this.auth.logout();
    this.cdRef.markForCheck();
  }

  public isUserLogged(): boolean {
    return this.auth.isAuth;
  }

  public currentUser(): Observable<User> {
    return this.auth.currentUser;
  }

  public ngOnDestroy(): void {
  }

}
