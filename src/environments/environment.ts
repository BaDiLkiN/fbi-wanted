export const environment = {
  production: false,
  logo: 'https://im0-tub-by.yandex.net/i?id=da7ef0ed3694f650041b642846f99993&n=13&exp=1',
  users: 'assets/users.json',
  posts: 'assets/posts.json',
  wantedURL: 'https://api.fbi.gov/wanted/v1/list',
  wantedDB: 'http://localhost:3000/updatedWantedData',
  apiURL: 'http://localhost:4200/api'
};
